$(document).ready(function(){
    /*************** MEMBERS Slider ******************/
    $('#members-slider').owlCarousel({
        margin: 25,
        smartSpeed: 1000,
        nav: false,
        dots: true,
        dotsEach: true,
        loop: true,
        autoplay: false,
        mouseDrag: true,
        touchDrag:true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    })

    /***************** blog slider ***********/

    $('#blog-content').owlCarousel({
        margin: 10,
        smartSpeed: 1000,
        nav: true,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        loop: true,
        autoplay: false,
        mouseDrag: true,
        touchDrag:true,
        responsive: {
            0: {
                items: 1
            },
            500: {
                items: 2
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    })
});